# Luhn algorithm with JUnit 5 testing

Luhn algorithm program optimized for testing and not performance.
Takes user input and lets the user know if the supplied number satisfies the luhn algorithm checksum.
Also checks if the number supplied is a valid credit card number.

Demo of program:

![demo of program](images/demo.PNG)


This projects uses JUnit 5 for testing and Gitlab CI runner. 