package luhn;

public class LuhnNode {

    private int input;

    public boolean isDoubleWeighted() {
        return isDoubleWeighted;
    }

    public void setDoubleWeighted(boolean doubleWeighted) {
        isDoubleWeighted = doubleWeighted;
    }

    private boolean isDoubleWeighted;
    private int sum;


    public LuhnNode(int input, boolean weight) {
        this.input = input;
        this.isDoubleWeighted = weight;
        this.sum = 0;
    }

    public LuhnNode(int input) {
        this.input = input;
        this.isDoubleWeighted = false;
        this.sum = 0;
    }

    public int getInput() {
        return input;
    }

    public void setInput(int input) {
        this.input = input;
    }



    public int getSum() {
        return sum;
    }

    public void setSum(int sum) {
        this.sum = sum;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if ( !(o instanceof LuhnNode) ) {
            return false;
        }

        LuhnNode node = (LuhnNode) o;

        if ( this.getInput() == node.getInput() && this.isDoubleWeighted() == node.isDoubleWeighted() ) {
            return true;
        }

        return false;
    }
    @Override
    public String toString(){
        return  "Input value: " + input + ", weight: " + isDoubleWeighted + ", sum: " + sum;
    }
}
