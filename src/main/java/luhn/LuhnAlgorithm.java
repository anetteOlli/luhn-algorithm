package luhn;

import java.util.LinkedList;

public class LuhnAlgorithm {



    /**
     *
     * @param digit single digit
     * @return doubles the input and if the result is greater than 9, subtract 9 from it.
     */
    public int multiplyBy2(int digit) {
        if (digit > 9) {
            throw new IllegalArgumentException("only single digit");
        }
        int result = digit * 2;

        if(result > 9) {
            result -= 9;
        }
        return result;
    }

    /**
     * Updates the numberList with sums
     * @param numberList list LuhnNodes that will have their sums calculated
     * NB: mutates the input.
     */
    public void generateSums(LinkedList<LuhnNode> numberList) {
        for (LuhnNode luhnNode : numberList) {
            if (luhnNode.isDoubleWeighted()) {
                luhnNode.setSum(multiplyBy2(luhnNode.getInput()));
            } else {
                luhnNode.setSum(luhnNode.getInput());
            }
        }
    }

    /**
     *
     * @param sums inkedList of luhnnodes
     * @return true if the sums satisfies the luhn algorithm
     */
    public boolean checkLuhnSum(LinkedList<LuhnNode> sums) {
        int result = sums.stream().mapToInt(LuhnNode::getSum).sum();
        result %= 10;
        return result == 0;
    }


    /**
     *
     * @param numberA string of a number
     * @param numberB number
     * @return true if the input numbers matches
     */
    public boolean compareStringToNumber(String numberA, int numberB) {
        return Integer.parseInt(numberA) == numberB;
    }

    /**
     * Calculates last digit that will satisfy the luhn algorithm
     * @param digitsWithSums including last digit
     * @return the checksum digit
     */
    public int calculateLastDigit(LinkedList<LuhnNode> digitsWithSums) {
        int result = digitsWithSums.stream().mapToInt(LuhnNode::getSum).sum();
        result *= 9;
        result %= 10;
        return result;
    }

    /**
     *
     * @param input in the format "*******"
     * @return list of LuhnNodes with input and weight, but without sums field set.
     */
    public LinkedList<LuhnNode> generateLuhnNodes(String input) {
        LinkedList<LuhnNode> nodes = new LinkedList<>();
        for(int i = 0; i < input.length(); i ++) {
            int digit = Integer.parseInt( "" + input.charAt(i));
            LuhnNode node = new LuhnNode( digit );
            nodes.add(node);
        }
        boolean hasDoubleWeight = true;
        for (int i = nodes.size() -1; i >= 0; i --) {
            nodes.get(i).setDoubleWeighted( hasDoubleWeight);
            hasDoubleWeight = !hasDoubleWeight;
        }
        return nodes;
    }

    /**
     *
     * @param input numbers as string
     * @return true if the length of the string is 16
     */
    public boolean hasCreditCardLenght(String input) {
        return input.length() == 16;
    }
}
