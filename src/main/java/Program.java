import luhn.LuhnAlgorithm;
import luhn.LuhnNode;

import java.util.LinkedList;
import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
        System.out.println("Write your number:");

        LuhnAlgorithm luhnAlgorithm = new LuhnAlgorithm();

        Scanner sc = new Scanner(System.in);
        String input = sc.next();
        String withOutLastDigit = "";
        String lastDigitString = "";
        if (input.length() > 2) {
            withOutLastDigit = input.substring(0, input.length() -1);
            lastDigitString = input.substring(input.length() -1, input.length());
        }
        LinkedList<LuhnNode> nodes = luhnAlgorithm.generateLuhnNodes(withOutLastDigit);
        luhnAlgorithm.generateSums(nodes);
        int calculatedLastDigit = luhnAlgorithm.calculateLastDigit(nodes);
        System.out.println("Input: " + withOutLastDigit + " " + lastDigitString);
        System.out.println("Provided: " + lastDigitString);
        System.out.println("Expected: " + calculatedLastDigit);
        System.out.println();

        String validString = luhnAlgorithm.compareStringToNumber(lastDigitString, calculatedLastDigit)? "valid" : "invalid";
        System.out.println("Checksum: " + validString);
        int inputLength = input.length();
        boolean isCreditCard = luhnAlgorithm.hasCreditCardLenght(input);
        String creditCardStatus = isCreditCard ? "(credit card)" : "";
        System.out.println("Digits: " + inputLength + " " + creditCardStatus);
    }
}
