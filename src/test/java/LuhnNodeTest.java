import luhn.LuhnNode;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class LuhnNodeTest {

    @Test
    void equalsTest() {
        LuhnNode node1 = new LuhnNode(1, true);
        LuhnNode node2 = new LuhnNode( 1, false);
        LuhnNode node3 = new LuhnNode( 1, true);
        LuhnNode node4 = new LuhnNode( 3, true);
        node3.setSum(2);

        assertTrue( node1.equals(node1));
        assertFalse( node1.equals( node2));
        assertTrue( node1.equals(node3));
        assertFalse( node1.equals(node4));

    }
}
