import luhn.LuhnAlgorithm;
import luhn.LuhnNode;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.LinkedList;

import static org.junit.jupiter.api.Assertions.*;

public class LuhnAlgorithmTest {

    private static LuhnAlgorithm luhnAlgorithm;
    private static LinkedList<LuhnNode> luhnNodes;
    private static LinkedList<LuhnNode> taskNodes;


    @BeforeAll
    static void setupLuhnNodeLinkedList() {
        luhnAlgorithm = new LuhnAlgorithm();
        luhnNodes = new LinkedList<LuhnNode>();
        luhnNodes.add(new LuhnNode(7, false));
        luhnNodes.add(new LuhnNode(9, true));
        luhnNodes.add(new LuhnNode(9, false));
        luhnNodes.add(new LuhnNode(2, true));
        luhnNodes.add(new LuhnNode(7, false));
        luhnNodes.add(new LuhnNode(3, true));
        luhnNodes.add(new LuhnNode(9, false));
        luhnNodes.add(new LuhnNode(8, true));
        luhnNodes.add(new LuhnNode(7, false));
        luhnNodes.add(new LuhnNode(1, true));

    }




    @Test
    void multiplyBy2Test() {
        assertEquals(4, luhnAlgorithm.multiplyBy2(2));
        assertEquals(7, luhnAlgorithm.multiplyBy2(8));
        assertNotEquals(14, luhnAlgorithm.multiplyBy2(7));
    }

    @Test
    void generateSums() {
        luhnAlgorithm.generateSums(luhnNodes);
        assertEquals(7, luhnNodes.get(0).getSum());
        assertEquals(9, luhnNodes.get(1).getSum());
        assertEquals(9, luhnNodes.get(2).getSum());
        assertEquals(4, luhnNodes.get(3).getSum());
        assertEquals(7, luhnNodes.get(4).getSum());
    }

    @Test
    void checkLuhnSum() {
        luhnAlgorithm.generateSums(luhnNodes);
        LuhnNode node = new LuhnNode(3, false);
        node.setSum(3);
        luhnNodes.add(node);
        assertTrue(luhnAlgorithm.checkLuhnSum(luhnNodes));
    }

    @Test
    void checkGenerateLuhnNodes() {
        LinkedList<LuhnNode> nodes1 =  luhnAlgorithm.generateLuhnNodes("7992739871");
        LinkedList<LuhnNode> nodes2 =  luhnAlgorithm.generateLuhnNodes("79927398715");
        assertTrue( nodes1.equals(luhnNodes));
        assertFalse( nodes2.equals(luhnNodes));
    }

    @Test
    void checkhasCreditCardLenght() {
        assertTrue( luhnAlgorithm.hasCreditCardLenght("1234567891234567"));
        assertFalse( luhnAlgorithm.hasCreditCardLenght( "123456789123456"));
        assertFalse( luhnAlgorithm.hasCreditCardLenght( "12345678912345678"));
    }

    @Test
    void checkcompareStringToNumber() {
        assertTrue( luhnAlgorithm.compareStringToNumber("7", 7));
        assertFalse( luhnAlgorithm.compareStringToNumber("9", 7));
    }

    @Test
    void checkCalculateLastDigit() {
        LinkedList<LuhnNode> firstNodes = luhnAlgorithm.generateLuhnNodes("7992739871");
        luhnAlgorithm.generateSums(firstNodes);
        LinkedList<LuhnNode> secondNodes = luhnAlgorithm.generateLuhnNodes("424242424242424");
        luhnAlgorithm.generateSums(secondNodes);

        assertEquals( 3, luhnAlgorithm.calculateLastDigit(firstNodes));
        assertEquals( 2, luhnAlgorithm.calculateLastDigit(secondNodes));
    }

}
